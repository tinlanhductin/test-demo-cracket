#!/bin/sh
while true
do
	echo "Committing..."
	build_number=$(cat updater.txt)
	build_number=$((build_number+1))
	echo $build_number > updater.txt
	git add .
	hash=$(openssl rand -base64 32)
	git commit -m "update #$build_number seed of $hash"
	total_account=10
	for i in $(seq 1 $total_account); do
		echo "Push for remote no$i"
		git push "no$i" master
	done
	echo "Build number #$build_number"
	duration=$(awk -v min=600 -v max=2900 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
	echo "Next commit in $duration seconds..."
	sleep $duration
done
